try {
	db.createUser({
		user: "devopsUser", 
		pwd: "pwd", 
		roles : [
			{
				role: "readWrite", 
				db: "academy"
			}
		]
	});
} catch (err) {
	print(err);
	quit(1);
}
