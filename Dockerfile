FROM mongo

ENV MONGO_INITDB_ROOT_USERNAME admin
ENV MONGO_INITDB_ROOT_PASSWORD password
ENV MONGO_INITDB_DATABASE academy

COPY ["setup_db.js", "setup_user.js", "/docker-entrypoint-initdb.d/"]

RUN echo "This is a special image for AC Academy training"
