
db.person.drop();

try {
  db.person.insertMany([
    { name: "Rose", age: 26, hobby: "Sleep"},
    { name: "Ed", age: 30, hobby: "Play Games"},
    { name: "Mary", age: 22, hobby: "Read Books"}
  ])
} catch(err) {
  print(err);
  quit(1);
}

