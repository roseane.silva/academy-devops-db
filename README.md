# academy-devops-db

This repository is for the specific Mongo database used for the **AC Academy - DevOps project**.

Since it's for training purposes, the db user and password are set on Dockerfile.
This image will run a script so Mongo db starts with a database called `academy`, a collection called `person` (this collection will be initialized with 3 documents), and a user called `devopsUser` - this user will be used by the application to connect with the database.

Image on Docker Hub: `roseanesilva/mongo-devops-academy`

How to run this app:

- Open Play with Docker
- Create a new instance

**Create a docker container:**

`docker run roseanesilva/mongo-devops-academy`

**Check container name:**

`docker ps`

**To login on Mongo shell:**

`docker exec -it [container_name] mongo -u admin -p password`

**To select the initialized "academy" database:**

`use academy`

**To check the initialized documents:**

`db.person.find()`
